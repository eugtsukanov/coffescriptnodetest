angular.module 'App', ['ngResource']

.factory 'User', ($resource) ->
  $resource '/users/me'

.controller 'UserCtrl', ($scope, User) ->
  $scope.user = User.get()