module.exports = (grunt) ->
  grunt.initConfig
    pkg: grunt.file.readJSON 'package.json'

    concurrent:
      start:
        tasks: ['watch', 'nodemon']
        options:
          logConcurrentOutput: true

    coffee:
      client:
        options:
          bare: true
          sourceMap: true
        expand: true
        flatten: false
        cwd: "client/js"
        src: ["**/*.coffee"]
        dest: 'client/js'
        ext: ".js"
      server:
        options:
          bare: true
          sourceMap: true
        expand: true
        flatten: false
        cwd: "server"
        src: ["**/*.coffee"]
        dest: 'server'
        ext: ".js"

    watch:
      coffee_client:
        files: ['client/js/**/*.coffee']
        tasks: ['coffee:client']
        options:
          spawn: false

      coffee_server:
        files: ['server/**/*.coffee']
        tasks: ['coffee:server']
        options:
          spawn: false

    nodemon:
      dev:
        script: 'server/app.js'
        options:
          watch: ['server']

  #Load Tasks
  grunt.loadNpmTasks 'grunt-contrib-coffee'
  grunt.loadNpmTasks 'grunt-contrib-watch'
  grunt.loadNpmTasks 'grunt-nodemon'
  grunt.loadNpmTasks 'grunt-concurrent'


  grunt.registerTask 'compile', ['coffee']
#  grunt.registerTask 'nodemon', ['']
  grunt.registerTask 'default', ['concurrent:start']
