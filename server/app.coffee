express = require 'express'
app = express()

app.use(express.static(__dirname + '/../client'))

app.get '/users/me', (req, res) ->
  user =
    name: 'Eric'
    role: 'user'

  res.json(user)

app.listen 3000, ->
  console.log 'started on 3000 port'