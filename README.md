## The task this project is for, is to create an ajax query on a page that checks if a user is a certain role. If so, it hides an html field. ##

### This is project has Vagrantfile, it help to create dev environment ###

* [Vagrant documentation](https://www.vagrantup.com/)
* rename file Vagrantfile.dist to Vagrantfile
* run $ vagrant up 
* run $ vagrant ssh
* run $ cd /home/vagrant/project
 

### Project prepare ###
 
* if you'll use Vagrant it should be automatically install main project dependencies, please check Vagrantfile
* for run project locally, you have to install some libs below:
    - node.js
    - nodemon | npm install -g nodemon
    - bower | npm install -g bower
    - cofee-script | npm install -g coffee-script
    - grunt | npm install -g grunt-cli


 
### Start project ###

* run $ npm i && bower i
* run $ grunt
* this is command run grunt watch and grunt nodemon it help automatically reload application after changes in files, and recompile coffee files to .js

### Check project ###

* server files you can find in 'server' folder
* all client files in 'client' folder
* for check hide or show input with 'secret' text you have to change user role in server/app.coffee file to 'admin'


